
export class ToDoListPage {
  constructor() {
    this.document = document

    this.items = []
  }

  render() {
    this.draw()

    this.addCallbacks()
  }

  add() {
    const newItem = this.document.getElementById('new-item-input').value

    this.items.push(newItem)
    this.render()
  }

  delete(index) {
    delete this.items[index]
    this.render()
  }

  //private

  draw() {
    const page = this.document.getElementById('page')
    page.innerHTML = `
      <input type="text" id="new-item-input"><button id="new-item-button">Add</button>
      ${this.itemsList()}
    `
  }

  itemsList() {
    const items = this.items.map((item, index) => `<li id = "delete-item-${index}" >${item}</li>`).join('')

    return `<ul>${items}</ul>`
  }

  addCallbacks() {
    
    const button = this.document.getElementById('new-item-button')
    button.addEventListener('click', this.add.bind(this))
    this.items.map((_,index) => this.document.getElementById('delete-item-' + index).addEventListener('click', this.delete.bind(this, index)) )
    

  }
}