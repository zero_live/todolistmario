
export class SpyDocument {
  constructor() {
    this.elements = {}
  }

  getElementById(id) {
    if (this.elements[id]) { return this.elements[id] }
    const element = new Element()

    this.elements[id] = element

    return element
  }

  type(id, text) {
    this.getElementById(id).type(text)
  }

  click(id) {
    this.elements[id].click()
  }

  includes(text) {
    const elements = Object.values(this.elements)
    const html = elements.map(element => element.innerHTML).join('')

    return html.includes(text)
  }
}

class Element {
  constructor() {
    this.listeners = {}

    this.innerHTML
    this.value
  }

  type(value) {
    this.value = value
  }

  click() {
    this.listeners.click()
  }

  addEventListener(event, callback) {
    this.listeners[event] = callback
  }
}