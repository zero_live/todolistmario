import { ToDoListPage } from '../public/src/ToDoListPage.js'
import { SpyDocument } from './utilities/SpyDocument.js'

describe('ToDoListPage', () => {
  it('adds an item to the list', () => {
    const newItem = 'someText'
    const page = new TestToDoListPage()

    page.type(newItem)
    page.add()

    expect(page.includes(newItem)).toEqual(true)
  })

  it('Deletes an item from list', () =>{
    const newItem = 'someText'
    const page = new TestToDoListPage()

    page.type(newItem)
    page.add()
    page.delete()

    expect(page.includes(newItem)).toEqual(false)
  })
})

class TestToDoListPage {
  constructor() {
    this.spyDocument = new SpyDocument()

    const page = new ToDoListPage()
    page.document = this.spyDocument
    page.render()
  }

  type(text) {
    this.spyDocument.type('new-item-input', text)
  }

  add() {
    this.spyDocument.click('new-item-button')
  }

  delete() {
    this.spyDocument.click('delete-item-0')
  }

  includes(text) {
    return this.spyDocument.includes(text)
  }
}